package pl.sda.task3;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class QuadraticFunctionTest {

    @Test
    void shouldHaveNoSolutionsWhenDeltaIsNegative() {
        //given
        //ustawiamy takie parametry wejściowe, aby spowodować ujemną deltę
        var a = 2;
        var b = 1;
        var c = 5;
        var quadraticFunction = new QuadraticFunction(a, b, c);

        //when
        //wywołujemy funkcję, którą testujemy
        double[] solutions = quadraticFunction.calculateSolutions();

        //then
        //sprawdzamy rezultat
        assertThat(solutions).isEmpty();
    }

    @Test
    void shouldHaveOnlyOneSolutionWhenDeltaIsZero() {
        //given
        //ustawiamy takie parametry wejściowe, aby spowodować zerową deltę
        var a = 1;
        var b = 2;
        var c = 1;
        var quadraticFunction = new QuadraticFunction(a, b, c);

        //when
        //wywołujemy funkcję, którą testujemy
        double[] solutions = quadraticFunction.calculateSolutions();

        //then
        //sprawdzamy rezultat
        assertThat(solutions).hasSize(1);
    }

    @Test
    void shouldHaveTwoSolutionsWhenDeltaIsPositive() {
        //given
        //ustawiamy takie parametry wejściowe, aby spowodować dodatnią deltę
        var a = 1;
        var b = 6;
        var c = 1;
        var quadraticFunction = new QuadraticFunction(a, b, c);

        //when
        double[] solutions = quadraticFunction.calculateSolutions();

        //then
        //sprawdzamy rezultat
        assertThat(solutions).hasSize(2);
    }
}
package pl.sda.task4;

import pl.sda.shared.IntNumber;

import java.util.Scanner;

class MainTask4 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a positive number: ");
        int number = scanner.nextInt();

        if (number <= 0) {
            System.out.println("Provided a negative number.");
            return;
        }

        IntNumber intNumber;
        for (int i = 1; i <= number; i++) {
            intNumber = new IntNumber(i);
            if (intNumber.isDivisibleBy(3) && intNumber.isDivisibleBy(7)) {
                System.out.println("Pif paf");
            } else if (intNumber.isDivisibleBy(7)) {
                System.out.println("paf");
            } else if (intNumber.isDivisibleBy(3)) {
                System.out.println("Pif");
            } else {
                intNumber.print();
            }
        }
    }
}

package pl.sda.task3;

import java.util.Scanner;

class MainTask3 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a: ");
        int a = scanner.nextInt();

        System.out.print("Enter b: ");
        int b = scanner.nextInt();

        System.out.print("Enter c: ");
        int c = scanner.nextInt();

        var quadraticFunction = new QuadraticFunction(a, b, c);
        double[] solutions = quadraticFunction.calculateSolutions();

        if (solutions.length == 0) {
            System.out.println("Negative delta, no solutions");
            return;
        }

        System.out.println("Solutions of the quadratic function:");
        for (double solution : solutions) {
            System.out.println(solution);
        }
    }
}

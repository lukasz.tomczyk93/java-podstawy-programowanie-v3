package pl.sda.task20;

import java.util.Random;
import java.util.Scanner;

class MainTask20 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        var random = new Random();
        int randomNumber = random.nextInt(101);
        int userNumber;

        do {
            System.out.print("Enter a number: ");
            userNumber = scanner.nextInt();
            if (userNumber > randomNumber) {
                System.out.println("Too big number");
            } else if (userNumber < randomNumber) {
                System.out.println("Too small number");
            }
        } while (userNumber != randomNumber);

        System.out.println("Bingo!");
    }
}

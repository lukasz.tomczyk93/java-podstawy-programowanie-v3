package pl.sda.task17;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

class MainTask17 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Provide next class date: ");
        String nextClassDateAsString = scanner.next();

        var polishFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate nextClassDate = LocalDate.parse(nextClassDateAsString, polishFormat);
        LocalDate now = LocalDate.now();

        long daysToNextClass = ChronoUnit.DAYS.between(now, nextClassDate);
        System.out.println(String.format("Days to next class: %s", daysToNextClass));
    }
}

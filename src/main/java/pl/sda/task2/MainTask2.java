package pl.sda.task2;

import java.util.Scanner;

class MainTask2 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter weight [kg]: ");
        var weight = scanner.nextFloat();

        System.out.print("Enter height [cm]: ");
        var height = scanner.nextInt();

        BMI bmi = new BMI(weight, height);
        bmi.print();

        var message = bmi.isOptimal() ? "BMI is optimal" : "BMI is not optimal";
        System.out.println(message);
    }
}

package pl.sda.task2;

class BMI {

    private final double bmiValue;

    BMI(float weight, int height) {
        float heightInMeter = height / 100F;
        bmiValue = weight / (heightInMeter * heightInMeter);
    }

    boolean isOptimal() {
        return bmiValue >= 18.5 && bmiValue <= 24.9;
    }

    void print() {
        System.out.println(String.format("BMI = %s", bmiValue));
    }
}

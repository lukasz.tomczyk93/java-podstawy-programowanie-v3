package pl.sda.task18;

import java.util.Scanner;
import java.util.regex.Pattern;

class MainTask18 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a text: ");
        var userText = scanner.nextLine();

        //Pattern.CASE_INSENSITIVE oznacza to,że nie interesuje nas wielkość liter
        var pattern = Pattern.compile("([a])+([ ])(psik)", Pattern.CASE_INSENSITIVE);
        var matcher = pattern.matcher(userText);
        boolean userSneezed = matcher.find();

        String result = userSneezed ? "Yes" : "No";
        System.out.println(result);
    }
}
package pl.sda.task10;

import java.util.Scanner;

class MainTask10 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter the number to calculate sum of its digits: ");
        int number = scanner.nextInt();

        if (number <= 0) {
            System.out.println("Provided a negative number.");
            return;
        }

        int sum = sumOfDigitsByTreatingNumberAsText(number);
        System.out.println(String.format("Sum of digits = %s", sum));
    }

    private static int sumOfDigitsByTreatingNumberAsText(int number) {
        int sum = 0;
        String numberAsText = String.valueOf(number);

//        The alternative option
//        char[] chars = numberAsText.toCharArray();
//        for (char aChar : chars) {
//            int digit = Character.getNumericValue(aChar);
//            sum += digit;
//        }

        for (int i = 0; i < numberAsText.length(); i++) {
            char digitAsChar = numberAsText.charAt(i);
            int digit = Character.getNumericValue(digitAsChar);
            sum += digit;
        }
        return sum;
    }

    private static int sumOfDigitsUsingModulo(int number) {
        int rest; //to jest nasza poszczególna cyfra w liczbie i ją będziemy dodawać do sumy przy każdym obrocie
        int intPart = number;
        int sum = 0; //nasza finalna suma, wiadomo, na początku 0

        while (intPart > 0) {
            rest = intPart % 10; //idziemy sobie od prawej do lewej z naszymi cyframi
            sum += rest; //...dodajemy...
            intPart = intPart / 10; /* To już jest specyfika naszego systemu liczbowego, którym się posługujemy na co dzień (jest to system dziesiętny).
                                         Dla nas jest to oczywiste, bo widzimy okiem te cyfry w liczbie, ale komputer tego nie widzi i tu go właśnie uczymy.
                                         Są jeszcze inne, np. dwójkowy (czyli binarny - dla komputera).
                                         Zachęcam do przerobienia jakiegoś przykładu na kartce papieru i podstawiania sobie tych wartości */
        }
        return sum;
    }
}

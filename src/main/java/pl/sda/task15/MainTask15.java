package pl.sda.task15;

import java.util.Scanner;

class MainTask15 {

    public static void main(String[] args) {
        //pobieramy 10 liczb z konsoli
        int[] numbers = getNumbersFromUser();

        //Główna pętla iteruje po każdym elemencie z tablicy
        for (int i = 0; i < numbers.length; i++) {
            int currentNumber = numbers[i];
            //wywołujemy funkcję, która liczy wystąpienia obecnie iterowanej wartości
            int numberOccurrences = countOccurrences(currentNumber, numbers);

            //tutaj upewniamy się, czy przypadkiem już nie sprawdziliśmy już danej liczby, żeby nie wyświetlać jej dwukrotnie
            boolean wasNumberNotChecked = !wasNumberAlreadyChecked(currentNumber, i, numbers);
            if (wasNumberNotChecked && numberOccurrences >= 2) {
                System.out.println(currentNumber);
            }
        }
    }

    private static boolean wasNumberAlreadyChecked(int currentNumber, int currentNumberIndex, int[] numbers) {
        //Ta pętla sprawdza, czy przypadkiem już nie sprawdzaliśmy wystąpień danej liczby
        for (int i = 0; i < currentNumberIndex; i++) {
            if (currentNumber == numbers[i]) {
                //jeśli okaże się, że już dana liczba była sprawdzona, to zwracamy "true" i jednocześnie przerywamy pętlę.
                return true;
            }
        }
        return false;
    }

    private static int countOccurrences(int number, int[] numbers) {
        int occurrences = 0;
        //tutaj sprawdzamy wystąpienia danej liczby za pomocą zmiennej pomocniczej "occurrences"
        for (int currentNumber : numbers) {
            if (number == currentNumber) {
                occurrences++;
            }
        }
        return occurrences;
    }

    private static int[] getNumbersFromUser() {
        var scanner = new Scanner(System.in);
        System.out.println("Enter ten numbers");
        int[] numbers = new int[10];

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(String.format("Number %s: ", i + 1));
            numbers[i] = scanner.nextInt();
        }

        return numbers;
    }
}

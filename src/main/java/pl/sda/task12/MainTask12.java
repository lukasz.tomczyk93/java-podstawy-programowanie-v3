package pl.sda.task12;

import java.util.Scanner;

class MainTask12 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a text: ");
        var theText = scanner.nextLine();

        if (theText.isEmpty()) {
            System.out.print("Provided an empty text");
            return;
        }

        char space = ' ';

        int spacesNumber = 0;
        for (int i = 0; i < theText.length(); i++) {
            char currentChar = theText.charAt(i);
            if (currentChar == space) {
                spacesNumber++;
            }
        }

        float spacesPart = ((float) spacesNumber / theText.length()) * 100;

        System.out.println(String.format("Spaces percentage: %s", spacesPart));
    }
}

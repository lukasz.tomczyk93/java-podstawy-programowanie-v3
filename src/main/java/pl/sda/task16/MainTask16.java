package pl.sda.task16;

import java.util.Scanner;

class MainTask16 {

    public static void main(String[] args) {
        int[] numbers = getUserInput();

        //Zmienna przetrzymująca informację o najdłuższym rosnącym podciągu (inicjalnie 1, żeby od czegoś zacząć)
        int maxSubsequenceLength = 1;

        //Zmienna przetrzymująca informację o długości obecnie sprawdzanego rosnącego podciągu (inicjalnie 1 żeby od czegoś zacząć)
        int currentIncreasingSubsequenceLength = 1;

        //Zaczynamy od 1 a nie od 0, żeby porównać obecny element z poprzednim
        for (int i = 1; i < numbers.length; i++) {
            //poróznujemy obecny element z poprzednim w celu sprawdzenia czy podciąg rośnie
            if (numbers[i - 1] < numbers[i]) {
                //jeśli rośnie to podbijamy zmienna pomocniczą o 1
                currentIncreasingSubsequenceLength++;
            } else {
                //jeżeli nie rośnie to znaczy że musimy zresetować zmienną pomocniczą żeby była gotowa na liczenie nowego rosnącego podciągu (jesli taki wystąpi)
                currentIncreasingSubsequenceLength = 1;
            }

            //jeśli się okaże, że obecnie sprawdzany podciąg jest dłuższy od poprzedniego, to wtedy ten obecny 'wygrywa' i on staje się najdłuższym ze wszystkich
            if (currentIncreasingSubsequenceLength > maxSubsequenceLength) {
                maxSubsequenceLength = currentIncreasingSubsequenceLength;
            }
            System.out.println();
        }
        System.out.println(String.format("The longest increasing subsequence has length: %s", maxSubsequenceLength));
    }

    //Pobieramy 10 liczb separując je średnikiem
    private static int[] getUserInput() {
        var scanner = new Scanner(System.in);
        System.out.print("Enter 10 numbers (separated by ';'): ");
        String inputAsText = scanner.next();
        String[] numbersAsStrings = inputAsText.split(";");
        int[] numbers = new int[10];

        for (int i = 0; i < numbersAsStrings.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsStrings[i]);
        }

        return numbers;
    }
}

package pl.sda.task11;

import java.util.Scanner;

class MainTask11 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        String inputText;

        //tutaj przechowamy najdłuższy tekst
        String theLongestText = "";

        //Wykonujemy pętlę, dopóki wpisany tekst jest inny niż "starczy" (nie patrząc na wielkość liter)
        String exitText = "starczy";
        do {
            System.out.print("Enter a text: ");
            inputText = scanner.nextLine();
            if (inputText.isBlank()) {
                System.out.println("Provided empty text. Good by!");
                return;
            }
            int currentTextLength = inputText.length();
            //1) Nie bierzemy pod uwagę samego teksty "starczy" - stąd ten pierwszy warunek w "if"
            //2) Jeśli się okaże, że nowy wprowadzony tekst jest dłuższy niż poprzedni najdłuższy, to wtedy ten nowy staje się najdłuższym tekstem
            if (!inputText.equalsIgnoreCase(exitText) && currentTextLength > theLongestText.length()) {
                theLongestText = inputText;
            }
        } while (!inputText.equalsIgnoreCase(exitText));

        System.out.println(String.format("The longest text was: %s", theLongestText));
    }
}

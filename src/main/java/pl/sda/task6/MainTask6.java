package pl.sda.task6;

import java.util.Scanner;

class MainTask6 {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.print("Enter a positive number to calculate its harmonic series: ");
        int number = scanner.nextInt();

        if (number <= 0) {
            System.out.println("Provided a negative number.");
            return;
        }

        var harmonicSeries = new HarmonicSeries(number);

        System.out.println(String.format("Harmonic series of %s = %s", number, harmonicSeries));
    }
}

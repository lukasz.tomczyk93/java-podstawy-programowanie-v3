package pl.sda.task1;

import java.util.Scanner;

class MainTask1 {

    public static void main(String[] args) {
        float diameter = getDiameter();
        var circle = new Circle(diameter);
        var circuit = circle.calculateCircuit();
        System.out.println(String.format("The circuit is %s", circuit));
    }

    private static float getDiameter() {
        System.out.print("Enter the diameter: ");
        var scanner = new Scanner(System.in);
        return scanner.nextFloat();
    }
}

package pl.sda.shared;

public class IntNumber {

    private final int value;

    public IntNumber(int value) {
        this.value = value;
    }

    public boolean isDivisibleBy(int divisor) {
        return value % divisor == 0;
    }

    public void print() {
        System.out.println(value);
    }

    public boolean isPrime() {
        //Liczba pierwsza to taka, która ma dwa dzielniki: "1" i samą siebie
        for (int i = 2; i < value; i++) {
            //Z góry zakładamy, że każda liczba jest pierwsza
            //i tutaj tylko sprawdzamy, czy ma jeszcze jakiś inny dzielnik
            if (value % i == 0) {
                //Jeśli ma, to wtedy nie jest pierwsza i od razu przerywamy funkcję, bo nie ma sensu sprawdzać dalej (np. czy ma 3 czy 200 dzielników - nadal nie jest pierwsza)
                return false;
            }
        }
        return true;
    }
}
